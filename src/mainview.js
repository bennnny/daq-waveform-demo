"use strict";
import React from "react";
import "./mainview.css";
import PerformanceDemo from "./darw-waveform";
// import Header from 'header';
// import TargetSel from 'targetsel';
// import TransArea from 'transarea';
// import Console from 'console';
import {
  Container,
  Segment,
  Button,
  Dimmer,
  Popup,
  Loader,
  Header,
  Grid
} from "semantic-ui-react";
import boardInfo from "../systemfile/boardinfo.js";
//import ItemSelect from '../component/itemselect.js';
import FileUpload from "../component/fileupload.js";
//import FileDownload from '../component/filedownload.js';
// import EventButton from '../component/eventbutton';
import { getRequest } from "../systemfile/ajaxhelper.js";

const style = {
  borderRadius: 0,
  opacity: 0.7,
  padding: "2em"
};
class MainView extends React.Component {
  constructor() {
    super();

    this.state = {
      boardUsed: "DAQ",
      building: false,
      undownladable: true,
      boards: boardInfo,
      boardItem: {},
      upgradeInfo: "",
      upgreadNotReady: true,
      dimmerActive: false,
      uploadInfo: "",
      renderInfo: {}
    };
    this.renderUpload = this.renderUpload.bind(this);
    this.creatBoardTypeItem = this.creatBoardTypeItem.bind(this);
    this.selectBoardType = this.selectBoardType.bind(this);
    this.buildEvent = this.buildEvent.bind(this);
    this.downloadEvent = this.downloadEvent.bind(this);
    this.loadFinish = this.loadFinish.bind(this);
    this.cleanAllFileInfo = this.cleanAllFileInfo.bind(this);
    this.showUploadMenu = this.showUploadMenu.bind(this);
  }

  loadFinish(e, index, fileObj) {
    // const boards ={ ...this.state.boards};

    console.log(e);
    console.log("this.state.boardUsed " + this.state.boardUsed);
    console.log("load finish " + index);
    console.log(fileObj);
    if (
      this.state.boardUsed === "DAQ" &&
      fileObj.name.toUpperCase() !== "DAQIMAGE.TGZ"
    ) {
      boardInfo[this.state.boardUsed][index].fileInfo =
        "file not accepted, please try another one";
      this.setState({
        boards: boardInfo,
        upgreadNotReady: true,
        renderInfo: Object.keys(
          this.state.boards[this.state.boardUsed || "DAQ"]
        ).map(key => this.renderUpload(key))
      });
    } else {
      boardInfo[this.state.boardUsed][index].fileInfo = fileObj.name;
      this.setState({
        boards: boardInfo,
        upgreadNotReady: false,
        renderInfo: Object.keys(
          this.state.boards[this.state.boardUsed || "DAQ"]
        ).map(key => this.renderUpload(key))
      });
    }
  }

  cleanAllFileInfo() {
    Object.keys(boardInfo[this.state.boardUsed]).map(key => {
      boardInfo[this.state.boardUsed][key].fileInfo = "";
    });
  }

  renderUpload(key) {
    if (this.state.boardUsed) {
      return (
        <FileUpload
          key={key}
          index={key}
          header={this.state.boards[this.state.boardUsed][key].header}
          url={`/uploads/${key}`}
          loadFinish={this.loadFinish}
          showInfo={this.state.boards[this.state.boardUsed][key].fileInfo}
        />
      );
    }
  }

  creatBoardTypeItem() {
    return Object.keys(this.state.boards).map(key => {
      return { value: key, text: key };
    });
  }

  buildEvent() {
    console.log("buildEvent");
    this.setState({
      building: true,
      dimmerActive: true,
      upgreadNotReady: true
    });
    getRequest(`/build/${this.state.boardUsed}`, (e, res) => {
      console.log(e);
      console.log(res);
      this.cleanAllFileInfo();
      this.setState({
        building: false,
        undownladable: false,
        boards: boardInfo,
        upgreadNotReady: true,
        dimmerActive: false,
        renderInfo: <Header as="h1">Please reboot the board </Header>
      });
    });
  }

  showUploadMenu() {
    return (
      <Segment basic>
        <Dimmer active={this.state.dimmerActive} page>
          <Header as="h2" inverted>
            <Loader inverted active={this.state.dimmerActive}>
              Please wait.
            </Loader>
          </Header>
        </Dimmer>
        <Segment basic>{this.state.renderInfo}</Segment>
      </Segment>
    );
  }

  downloadEvent() {
    console.log("download file");
    this.setState({ undownladable: true });
  }

  componentWillMount() {
    const boardItem = this.creatBoardTypeItem();
    this.setState({
      boardItem,
      renderInfo: Object.keys(
        this.state.boards[this.state.boardUsed || "DAQ"]
      ).map(key => this.renderUpload(key))
    });
  }

  upgradeButton() {
    return (
      <Button
        color="green"
        floated="right"
        onClick={this.buildEvent}
        loading={this.state.building}
        content="Upgrade Now"
        disabled={this.state.upgreadNotReady}
      />
    );
  }

  selectBoardType(e, selObj) {
    getRequest(`/setboard/${selObj.value}`, (e, res) => {
      console.log(e);
      console.log(res);
      this.setState({ boardUsed: selObj.value });
    });
  }

  render() {
    return (
      <div>
        <Segment basic inverted />
        <Container>
          <Grid>
            <Grid.Row>
              <PerformanceDemo />
            </Grid.Row>
          </Grid>
          <Segment basic className="infomastion-seg">
            {this.showUploadMenu()}
          </Segment>
          <div>
            <Popup
              trigger={this.upgradeButton()}
              content="Upgrade will take a little time to update image, press 'Upgrade now'
            to confirm"
              style={style}
              inverted
            />
          </div>

          {this.state.upgradeInfo}
        </Container>
      </div>
    );
  }
}

export default MainView;
