"use strict";
var express = require("express");
var net = require("net");
var debug = require("debug");
var log = debug("base:log");
// var info = debug('base:info');
// var error = debug('base:error');
var app = express();
//var multer = require("multer");

// var fs = require("fs");

// var child_process = require("child_process");
// var GET_SOFTWARE_VERSION = 6;
// var GET_RAWDATA = 7;
var socketObj = {};
var connectState = false;
var apiResponse = {};
const WAVEFORM_PATTEN = "Waveform;";
const BLOCK_DATA_PATTEN = "#510000";
const WAVEFORM_PATTEN_LENGTH = WAVEFORM_PATTEN.length;
const BLOCK_DATA_PATTEN_LENGTH = BLOCK_DATA_PATTEN.length;
var daqInfo = {
  daqHeadReady: false,
  daqHead: "",
  recData: Buffer.allocUnsafe(8192).fill(0, 0),
  recCount: 0,
  dataCount: 0,
  header: "",
  headerlen: 0,
  dataValidLen: 0,
  tmpbuffer: Buffer.allocUnsafe(8192).fill(0, 0),
  tmplength: 0,
  blockData: []
};
const shiftBufferData = function(buf, offset, length) {
  let tbuf = Buffer.allocUnsafe(length);

  buf.copy(tbuf, 0, offset, offset + length);
  buf.fill(0);
  const len = tbuf.copy(buf, 0);
  tbuf = null;

  return len;
};
const getHeaderInfo = function(patten, src) {
  const strArray = src.split(";");
  let matchString = null;
  let matchArray;

  for (let i = 0, len = strArray.length; i < len; i += 1) {
    if (strArray[i].search(patten) >= 0) {
      matchString = strArray[i];
      break;
    }
  }

  if (matchString) {
    // matchArray = matchString.split(':')[1];
    return matchString.split(":")[1];
  }
  return null;
};
const saveDAQinfo = function(info) {
  // pdInfo.header
  const daqInfo = info;
  // let fd;
  // const jstring = '';
  const pdObj = { header: {}, rawdata: [] };

  try {
    pdObj.header.date = 0;
    pdObj.header.chNum = Number(
      getHeaderInfo("Channel Number", daqInfo.daqHead)
    );
    pdObj.header.startTime = 0;
    pdObj.header.offset = 0;
    pdObj.header.sampleRate = Number(
      getHeaderInfo("Sample Period", daqInfo.daqHead)
    );
    pdObj.header.vScale = Number(
      getHeaderInfo("Voltage Scale", daqInfo.daqHead)
    );
    pdObj.header.vHigh = 0;

    console.log(getHeaderInfo("Sample Period", daqInfo.daqHead));
    console.log(getHeaderInfo("Voltage Scale", daqInfo.daqHead));

    // pdObj.rawdata = daqInfo.recData;

    for (let i = 0, len = daqInfo.recCount; i < len; ) {
      // pdObj.rawdata.push(daqInfo.recData.readInt8(i));
      pdObj.rawdata.push(daqInfo.recData.readInt16LE(i));
      i += 2;
    }
    daqInfo.blockData.push(pdObj);
    // jstring = JSON.stringify(pdObj);
    // fd = fs.openSync(`./waveform/test-${pdObj.header.date}-${pdObj.header.startTime}`, 'w+');
    // fs.writeSync(fd, jstring, 0, jstring.length);
    // myNode.status({fill:"green",shape:"dot",text:"save ok"});
  } catch (e) {
    console.log(e);
    // The main node definition - most things happen in here
  }
};
var rawDataProcess = function(data, res) {
  let dataLength;
  let leftLength = data.length;
  let state = false;
  // log(`data length = ${data.length}`);
  // log(data);
  while (leftLength) {
    if (daqInfo.daqHeadReady === false) {
      let wfOffset;
      let recStr = "";

      // Waveform Data
      log("check header");
      log(`daqInfo.tmplength = ${daqInfo.tmplength}`);

      dataLength = data.copy(
        daqInfo.tmpbuffer,
        daqInfo.tmplength,
        data.length - leftLength,
        data.length
      );
      log(`1: coped dataLength = ${dataLength}`);

      leftLength -= dataLength;
      daqInfo.tmplength += dataLength;
      recStr = daqInfo.tmpbuffer.toString();
      wfOffset = recStr.search(WAVEFORM_PATTEN);
      if (wfOffset !== -1) {
        let tstr;

        daqInfo.daqHead = recStr.slice(0, wfOffset);

        log(`daqHead: ${daqInfo.daqHead}`);
        log(`wfOffset = ${wfOffset}`);
        log(`daqInfo.tmplength = ${daqInfo.tmplength}`);
        log("-------------------- check pd header ----------------");
        daqInfo.daqHeadReady = true;
        daqInfo.blockData = [];
        if (
          daqInfo.tmplength >=
          wfOffset + WAVEFORM_PATTEN_LENGTH + BLOCK_DATA_PATTEN_LENGTH
        ) {
          tstr = recStr.slice(wfOffset + WAVEFORM_PATTEN_LENGTH);
          // find #510000 patten
          if (tstr[0] === "#") {
            const num = Number(tstr[1]);
            const numstr = tstr.slice(2, Number(tstr[1]) + 2);

            if (Number.isNaN(tstr[1])) {
              log("waveform header format not correct");
              return false;
            }
            daqInfo.headerlen = num + 2;
            log(`numstr=${numstr}`);
            // block data length + NL
            daqInfo.dataCount = parseInt(numstr, 10);

            log(`daqInfo.dataCount = ${daqInfo.dataCount}`);

            delete daqInfo.recData;
            daqInfo.recData = Buffer.allocUnsafe(daqInfo.dataCount);

            wfOffset = wfOffset + WAVEFORM_PATTEN_LENGTH + daqInfo.headerlen;
            // check if more than one pd information
            if (daqInfo.tmplength >= daqInfo.dataCount + wfOffset) {
              daqInfo.daqHeadReady = false;
              dataLength = daqInfo.tmpbuffer.copy(
                daqInfo.recData,
                daqInfo.recCount,
                wfOffset,
                wfOffset + daqInfo.dataCount
              );
              if (dataLength === 0) {
                log(`2: copy faile = ${dataLength}`);
                daqInfo.recData = new Buffer.allocUnsafe(daqInfo.dataCount);
                dataLength = daqInfo.tmpbuffer.copy(
                  daqInfo.recData,
                  daqInfo.recCount,
                  wfOffset,
                  wfOffset + daqInfo.dataCount
                );
              }
              log(`2: coped dataLength = ${dataLength}`);

              // log(`2: daqInfo.recCount = ${daqInfo.recCount}`);
              // log(`2: wfOffset = ${wfOffset}`);
              // log(`2: wfOffset + daqInfo.dataCount =
              //    ${wfOffset + daqInfo.dataCount}`);

              daqInfo.recCount = dataLength;
              log("saveDAQinfo 22");
              saveDAQinfo(daqInfo);
              daqInfo.recCount = 0;
              state = true;
              if (daqInfo.tmplength > daqInfo.dataCount + wfOffset) {
                log("-------------------- continue ----------------");
                wfOffset += daqInfo.dataCount;
                daqInfo.tmplength = shiftBufferData(
                  daqInfo.tmpbuffer,
                  wfOffset,
                  daqInfo.tmplength - wfOffset
                );
                daqInfo.dataCount = 0;
              } else {
                // save pd data to file and return;
                daqInfo.tmpbuffer.fill(0);
                daqInfo.tmplength = 0;
                daqInfo.dataCount = 0;
                // return;
              }
            } else {
              dataLength = daqInfo.tmpbuffer.copy(
                daqInfo.recData,
                daqInfo.recCount,
                wfOffset,
                daqInfo.tmplength
              );
              log(`3: coped dataLength = ${dataLength}`);
              daqInfo.recCount = dataLength;
              daqInfo.tmpbuffer.fill(0);
              daqInfo.tmplength = 0;
              // return;
            }
          } else {
            log(`!! err : cant find # tstr[0] = ${tstr[0]}`);
            daqInfo.tmplength = 0;
            daqInfo.header = "";
            daqInfo.daqHeadReady = false;
            return false;
          }
        } else {
          // header check done , but '#510000' pattern not complete reciver.
          wfOffset += WAVEFORM_PATTEN_LENGTH;
          try {
            daqInfo.tmplength = shiftBufferData(
              daqInfo.tmpbuffer,
              wfOffset,
              daqInfo.tmplength - wfOffset
            );
          } catch (e) {
            console.log(e);
          }
          // return;
        }
      } else {
        daqInfo.tmplength = 0;
        log("warning :Check header format for No PD data reponse");

        if (recStr.search("No PD data") !== -1) {
          log("No PD data");
          state = true;
          // return true;
        }
      }
    } else if (daqInfo.dataCount === 0) {
      dataLength = data.copy(
        daqInfo.tmpbuffer,
        daqInfo.tmplength,
        data.length - leftLength,
        data.length
      );
      leftLength -= dataLength;

      // check waveform header '#xxxxx'
      daqInfo.tmplength += dataLength;

      daqInfo.header = daqInfo.tmpbuffer.slice(0, 20).toString();
      log(`data length = ${data.length}`);
      log(`daqInfo.tmplength = ${daqInfo.tmplength}`);
      // log('daqInfo.tmpbuffer = '  + daqInfo.tmpbuffer);
      // log('daqInfo.header = '  + daqInfo.header);
      log("-------------------- check waveform header ----------------");

      if (daqInfo.header[0] === "#") {
        const num = Number(daqInfo.header[1]);
        const numstr = daqInfo.header.slice(2, Number(daqInfo.header[1]) + 2);

        daqInfo.headerlen = num + 2;
        log(`numstr=${numstr}`);
        // block data length + NL
        daqInfo.dataCount = parseInt(numstr, 10);

        log(`daqInfo.dataCount = ${daqInfo.dataCount}`);

        delete daqInfo.recData;
        daqInfo.recData = Buffer.allocUnsafe(daqInfo.dataCount);

        if (daqInfo.tmplength >= daqInfo.dataCount + daqInfo.headerlen) {
          daqInfo.daqHeadReady = false;

          dataLength = daqInfo.tmpbuffer.copy(
            daqInfo.recData,
            daqInfo.recCount,
            daqInfo.headerlen,
            daqInfo.headerlen + daqInfo.dataCount
          );

          daqInfo.recCount = dataLength;
          // ////////////////
          if (dataLength === 0) {
            log(`33: copy faile = ${dataLength}`);
            daqInfo.recData = Buffer.allocUnsafe(daqInfo.dataCount);
            dataLength = daqInfo.tmpbuffer.copy(
              daqInfo.recData,
              daqInfo.recCount,
              daqInfo.headerlen,
              daqInfo.headerlen + daqInfo.dataCount
            );
          }
          daqInfo.recCount = dataLength;

          log(`33: copy dataLength = ${dataLength}`);
          // log(`33: daqInfo.recCount = ${daqInfo.recCount}`);
          // log(`33: daqInfo.headerlen = ${daqInfo.headerlen}`);
          // log(`33: daqInfo.headerlen + daqInfo.dataCount =
          //      ${daqInfo.headerlen + daqInfo.dataCount}`);

          saveDAQinfo(daqInfo);
          daqInfo.recCount = 0;
          state = true;
          if (daqInfo.tmplength > daqInfo.dataCount + daqInfo.headerlen) {
            log("-------------------- continue ----------------");
            daqInfo.tmplength = shiftBufferData(
              daqInfo.tmpbuffer,
              daqInfo.headerlen,
              daqInfo.tmplength - daqInfo.headerlen
            );
            daqInfo.dataCount = 0;
          } else {
            daqInfo.tmpbuffer.fill(0);
            daqInfo.tmplength = 0;
            daqInfo.dataCount = 0;
            // return;
          }
        } else {
          dataLength = daqInfo.tmpbuffer.copy(
            daqInfo.recData,
            daqInfo.recCount,
            daqInfo.headerlen,
            daqInfo.tmplength
          );
          log(`4: coped dataLength = ${dataLength}`);
          daqInfo.recCount = dataLength;
          daqInfo.tmpbuffer.fill(0);
          daqInfo.tmplength = 0;
          // return;
        }
      } else {
        daqInfo.tmplength = 0;
        daqInfo.header = "";
        daqInfo.daqHeadReady = false;
        return false;
      }
    } else {
      log("-------------------- check rawdata end ----------------");
      log(`daqInfo.recCount=${daqInfo.recCount},data.length= ${data.length}`);
      dataLength = data.copy(
        daqInfo.recData,
        daqInfo.recCount,
        data.length - leftLength,
        data.length
      );
      leftLength -= dataLength;
      log(`6: coped dataLength = ${dataLength}`);

      daqInfo.recCount += dataLength;

      if (daqInfo.recCount === daqInfo.dataCount) {
        log("saveDAQinfo 11");
        saveDAQinfo(daqInfo);
        state = true;
        daqInfo.daqHeadReady = false;
        daqInfo.recCount = 0;
        daqInfo.dataCount = 0;
        daqInfo.header = "";
      }
    }
  }

  if (state) {
    console.log("finish deconde");
    res.status(200).send(daqInfo.blockData);
  }
  return state;
  // if (data[data.length] === 0x0a) {
  //   res.status(200).send("OK");
  // }
};
var dataHandler = function(data) {
  // console.log("dataHandler:");
  // console.log(data.toString());
  if (apiResponse.dataFormat === "rawdata") {
    rawDataProcess(data, apiResponse.res);
  } else {
    console.log("apiResponse");
    apiResponse.res.json(data.toString());
  }
};

var connectDAQ = function(port, hostAddr, onData, onConnected) {
  if (connectState) {
    return false;
  }
  // console.log("connectDAQ");
  socketObj = net.connect(port, hostAddr, function() {
    //'connect' listener
    log("connected to server!");
    if (onConnected) onConnected();
  });

  socketObj.on("data", onData);

  socketObj.on("close", function(e) {
    log("onTcpConnect: close!");
    socketObj = {};
    connectState = false;
  });
  return true;
};

app.use("/", express.static(__dirname + "/build"));

app.get("/", function(req, res) {
  res.sendFile(__dirname + "/build/index.html");
});

app.get("/adcrange", function(req, res) {
  // console.log(req.query.rangeIndex);
  if (connectState) {
    apiResponse.res = res;
    apiResponse.dataFormat = "string";
    socketObj.write(
      JSON.stringify({
        cmd: 1,
        para: req.query.rangeIndex,
        chSrc: 1
      })
    );
  } else {
    res.status(400).send("not connected");
  }
});

app.get("/samplerate", function(req, res) {
  // console.log(req.query.samplerate);
  if (connectState) {
    apiResponse.res = res;
    apiResponse.dataFormat = "string";
    socketObj.write(
      JSON.stringify({
        cmd: 3,
        para: req.query.samplerate,
        chSrc: 1
      })
    );
  } else {
    res.status(400).send("not connected");
  }
});

app.get("/exttrig", function(req, res) {
  // console.log(req.query.extOnOff);
  if (connectState) {
    apiResponse.res = res;
    apiResponse.dataFormat = "string";
    socketObj.write(
      JSON.stringify({
        cmd: 17,
        para: req.query.extOnOff,
        chSrc: 1
      })
    );
  } else {
    res.status(400).send("not connected");
  }
});

app.get("/aquisition", function(req, res) {
  // console.log(req.query.chNum);
  // console.log(req.query.action);
  if (connectState) {
    apiResponse.res = res;
    apiResponse.dataFormat = "string";
    socketObj.write(
      JSON.stringify({
        cmd: 2,
        para: req.query.action,
        chSrc: 1
      })
    );
  } else {
    res.status(400).send("not connected");
  }
});

app.get("/memlength", function(req, res) {
  // console.log(req.query.length);
  if (connectState) {
    apiResponse.res = res;
    apiResponse.dataFormat = "string";
    socketObj.write(
      JSON.stringify({
        cmd: 9,
        para: req.query.length,
        chSrc: 1
      })
    );
  } else {
    res.status(400).send("not connected");
  }
});

app.get("/dataready", function(req, res) {
  // console.log(req.query.chNum);
  if (connectState) {
    apiResponse.res = res;
    apiResponse.dataFormat = "string";
    socketObj.write(
      JSON.stringify({
        cmd: 13,
        para: "",
        chSrc: Number(req.query.chNum)
      })
    );
  } else {
    res.status(400).send("not connected");
  }
});

app.get("/rawdata", function(req, res) {
  // console.log(req.query.chNum);
  if (connectState) {
    apiResponse.res = res;
    apiResponse.dataFormat = "rawdata";
    socketObj.write(
      JSON.stringify({
        cmd: 7,
        para: "",
        chSrc: Number(req.query.chNum)
      })
    );
  } else {
    res.status(400).send("not connected");
  }
});

app.get("/version", function(req, res) {
  if (connectState) {
    apiResponse.res = res;
    apiResponse.dataFormat = "string";
    socketObj.write(
      JSON.stringify({
        cmd: 6,
        para: "",
        chSrc: 1
      })
    );
  } else {
    res.status(400).send("not connected");
  }
});

app.get("/connect", function(req, res) {
  // console.log(req.query.portNum);
  // console.log(req.query.ipAddr);

  try {
    connectDAQ(req.query.portNum, req.query.ipAddr, dataHandler, function() {
      console.log("connect to 192.168.2.9");
      connectState = true;
      res.status(200).send("OK");
    });
  } catch (e) {
    res.status(400).send("ip not reachable");
  }
});

app.get("/disconnect", function(req, res) {
  // console.log(req.query.portNum);
  // console.log(req.query.ipAddr);

  if (connectState) {
    socketObj.end();
  }
  res.status(200).send("OK");
});

app.get("/users", function(req, res) {
  res.json([
    {
      id: 1,
      name: "one"
    },
    {
      id: 2,
      name: "two"
    }
  ]);
});

app.listen(8088, function() {
  console.log("listen on port 8088");
});
