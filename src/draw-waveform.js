import React, { Component } from "react";
import CanvasJSReact from "./assets/canvasjs.react";
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

class PerformanceDemo extends Component {
  componentDidMount() {}

  render() {
    var limit = 50000;
    var y = 100;
    var data = [];
    var dataSeries = { type: "line" };
    var dataPoints = [];

    if (this.props.rawData.length) {
      dataSeries.dataPoints = this.props.rawData;
      data.push(dataSeries);
    } else {
      for (var i = 0; i < limit; i += 1) {
        // y += Math.round(Math.random() * 10 - 5);
        dataPoints.push({
          x: i,
          y: 0
        });
      }
      dataSeries.dataPoints = dataPoints;
      data.push(dataSeries);
    }

    const spanStyle = {
      fontSize: "20px",
      fontWeight: "bold",
      backgroundColor: "#d85757",
      padding: "2px 4px",
      color: "#ffffff"
    };

    const options = {
      zoomEnabled: true,
      animationEnabled: true,
      // title: {
      //   text: "Try Zooming - Panning"
      // },
      axisY: {
        includeZero: false
      },
      data: data // random data
    };

    return (
      <div>
        <CanvasJSChart options={options} onRef={ref => (this.chart = ref)} />
        {/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
      </div>
    );
  }
}

export default PerformanceDemo;
