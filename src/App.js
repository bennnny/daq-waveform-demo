import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import PerformanceDemo from "./draw-waveform.js";
import "semantic-ui-css/semantic.min.css";
import {
  Container,
  Segment,
  Button,
  Dimmer,
  Popup,
  Loader,
  Header,
  Form,
  Select,
  Input,
  Divider,
  Menu,
  Icon,
  Modal,
  Grid
} from "semantic-ui-react";
import { reject } from "q";

const sampleRateOptions = [
  { key: "1", text: "80MSPS", value: "80000000" },
  { key: "2", text: "40MSPS", value: "40000000" },
  { key: "3", text: "20MSPS", value: "20000000" },
  { key: "4", text: "10MSPS", value: "10000000" },
  { key: "5", text: "5MSPS", value: "5000000" },
  { key: "6", text: "1MSPS", value: "1000000" },
  { key: "7", text: "100KSPS", value: "100000" },
  { key: "8", text: "10KSPS", value: "10000" },
  { key: "9", text: "1KSPS", value: "1000" }
];
const vRangeOptions = [
  { key: "1", text: "+- 1V", value: "0" },
  { key: "2", text: "+- 2.5V", value: "1" },
  { key: "3", text: "+- 5V", value: "2" },
  { key: "4", text: "+- 10V", value: "3" }
];
const memLengthOptions = [
  { key: "1", text: "10M Points", value: "20000000" },
  { key: "2", text: "1M Points", value: "2000000" },
  { key: "3", text: "100K Points", value: "200000" },
  { key: "4", text: "10K Points", value: "20000" },
  { key: "5", text: "1K Points", value: "2000" }
];
const triggerSourceOptions = [
  { key: "1", text: "Internal", value: "OFF" },
  { key: "2", text: "External", value: "ON" }
];

let ipAddress = "192.168.2.9";

class App extends Component {
  static queryChNum = 1;
  state = {
    users: [],
    runStop: "Run",
    runStopColor: "green",
    single: "Single",
    formDisabled: false,
    connectIcon: "cogs",
    connectState: "disconnect",
    functionDisable: true,
    disableSingle: false,
    disableRunStop: false,
    modalOpen: false,
    runState: "single",
    singleLoading: false,
    runLoading: false,
    iperror: false,
    rawData: []
  };

  triggerCheckDataReady = () => {
    console.log("triggerCheckDataReady");
    fetch(`/dataready?chNum=${App.queryChNum}`)
      .then(res => res.json())
      .then(data => {
        console.log(data);
        if (data === "DATA_READY") {
          console.log("try to fetch rawdata");
          fetch(`/rawdata?chNum=${App.queryChNum}`)
            .then(res => res.json())
            .then(data => {
              let tempRawdata = [];
              let rawdata = data[0].rawdata;
              // let rawHeader = data[0].header
              let sampleRate = 1 / data[0].header.sampleRate;
              let vScale = data[0].header.vScale;
              let timeSpace = 0;
              console.log(data[0]);

              for (let i = 0, len = rawdata.length; i < len; i++) {
                // for (let i = 0, len = 1000; i < len; i++) {
                tempRawdata.push({ x: timeSpace, y: rawdata[i] * vScale });
                timeSpace += sampleRate;
              }
              if (this.state.runState === "single") {
                this.setState({
                  rawData: tempRawdata,
                  disableSingle: false,
                  disableRunStop: false,
                  singleLoading: false
                });
              } else {
                //this.triggerCheckDataReady();
                if (this.state.runState === "run") {
                  this.setState({ rawData: tempRawdata });
                  fetch(`/aquisition?action=ON&chNum=${App.queryChNum}`)
                    .then(res => res.json())
                    .then(data => {
                      console.log("run issue acquisition command");
                      this.triggerCheckDataReady();
                    })
                    .catch(e => {
                      console.log("handleClickSingle error");
                      console.log(e);
                    });
                } else {
                  this.setState({ rawData: tempRawdata, runLoading: false });
                }
              }
            });
        } else if (data === "DATA_NOT_READY") {
          if (this.state.runState !== "stop") {
            this.triggerCheckDataReady();
          }
        }
      })
      .catch(e => {
        console.log("triggerCheckDataReady error");
        console.log(e);
      });
  };

  handleChange = (e, { searchInput, value }) => {
    console.log(searchInput.id);
    console.log(value);
    let cmdString = "";

    if (searchInput.id === "form-select-memory-length") {
      cmdString = `/memlength?length=${value}`;
    } else if (searchInput.id === "form-select-trigger-source") {
      cmdString = `/exttrig?extOnOff=${value}`;
    } else if (searchInput.id === "form-select-sampleing-rate") {
      cmdString = `/samplerate?samplerate=${value}`;
    } else if (searchInput.id === "form-select-vertical-range") {
      cmdString = `/adcrange?rangeIndex=${value}`;
    }
    fetch(cmdString)
      .then(res => res.json())
      .then(data => {
        console.log(`fetch ${cmdString} ${data}`);
      })
      .catch(e => {
        console.log(`fetch ${cmdString} error`);
      });
  };

  handleClickRunStop = (e, { children }) => {
    console.log(children[1]);
    if (children[1] === "Run") {
      this.setState({
        runStop: "Stop",
        runStopColor: "red",
        runState: "run",
        runLoading: true,
        formDisabled: true
      });
      fetch(`/aquisition?action=ON&chNum=${App.queryChNum}`)
        .then(res => res.json())
        .then(data => {
          console.log(data);
          this.setState({ disableSingle: true });
          this.triggerCheckDataReady();
        })
        .catch(e => {
          console.log("handleClickSingle error");
          console.log(e);
        });
    } else {
      this.setState({
        runStop: "Run",
        runStopColor: "green",
        runState: "stop",
        formDisabled: false,
        disableSingle: false
      });
    }
  };

  handleClickSingle = (e, { children }) => {
    fetch(`/aquisition?action=ON&chNum=${App.queryChNum}`)
      .then(res => res.json())
      .then(data => {
        console.log(data);
        this.setState({
          disableRunStop: true,
          disableSingle: true,
          singleLoading: true,
          runState: "single"
        });
        this.triggerCheckDataReady();
      })
      .catch(e => {
        console.log("handleClickSingle error");
        console.log(e);
      });

    console.log(children);
  };

  handleIPaddress = (e, { value }) => {
    //console.log(value);
    ipAddress = value;
  };

  handleModalOpen = () => {
    this.setState({ modalOpen: true });
  };

  timeoutPromise(ms, promise) {
    return new Promise((resolve, reject) => {
      const timeoutId = setTimeout(() => {
        reject("promise timeout");
      }, ms);
      promise.then(
        res => {
          clearTimeout(timeoutId);
          resolve(res);
        },
        err => {
          clearTimeout(timeoutId);
          reject(err);
        }
      );
    });
  }

  handleModalClose = () => {
    console.log(ipAddress);
    // fetch(`/connect?ipAddr=${ipAddress}&portNum=3030`, { method: "GET" })
    this.timeoutPromise(
      5000,
      fetch(`/connect?ipAddr=${ipAddress}&portNum=3030`, { method: "GET" })
    )
      .then(res => {
        console.log(res);
        console.log(res.status);
        //this.setState({ modalOpen: false });
        if (res.status === 200) {
          this.setState({ modalOpen: false, functionDisable: false });
        } else {
          this.setState({ modalOpen: false, iperror: true });
          reject();
        }
      })
      .catch(e => {
        console.log(e);
        console.log("connect to host fell");
        this.setState({ modalOpen: false, iperror: true });
      });
  };

  handleModalIpErrorClose = () => {
    this.setState({ iperror: false });
  };
  handleModalQuit = () => {
    this.setState({ modalOpen: false });
  };

  componentDidMount() {
    fetch("/disconnect").then(() => {
      this.setState({ functionDisable: true });
    });
  }
  render() {
    return (
      <div className="App">
        <Modal open={this.state.iperror}>
          <Modal.Content>
            <Modal.Description>
              <p>Can't connect to daq</p>
            </Modal.Description>
          </Modal.Content>
          <Modal.Actions>
            <Button
              color="green"
              onClick={this.handleModalIpErrorClose}
              inverted
            >
              <Icon name="checkmark" /> OK
            </Button>
          </Modal.Actions>
        </Modal>
        <Segment basic inverted clearing>
          <Modal
            trigger={
              <Button
                floated="right"
                circular
                color="orange"
                icon
                onClick={this.handleModalOpen}
              >
                <Icon name={this.state.connectIcon} />
              </Button>
            }
            open={this.state.modalOpen}
            onClose={this.handleClose}
          >
            <Modal.Header>Validate Host IP Address</Modal.Header>
            <Modal.Content>
              <Modal.Description>
                <p>DAQ IP Address</p>
                <Input
                  defaultValue="192.168.2.9"
                  onChange={this.handleIPaddress}
                />
              </Modal.Description>
            </Modal.Content>
            <Modal.Actions>
              <Button color="red" onClick={this.handleModalQuit} inverted>
                <Icon name="close" /> Quit
              </Button>
              <Button color="green" onClick={this.handleModalClose} inverted>
                <Icon name="checkmark" /> Validate
              </Button>
            </Modal.Actions>
          </Modal>
        </Segment>
        <Segment vertical disabled={this.state.functionDisable}>
          <Grid divided>
            <Grid.Row>
              <Grid.Column width={14}>
                <PerformanceDemo rawData={this.state.rawData} />
              </Grid.Column>
              <Grid.Column width={2}>
                <Grid.Row verticalAlign="top">
                  <Button
                    disabled={this.state.disableRunStop}
                    fluid
                    color={this.state.runStopColor}
                    onClick={this.handleClickRunStop}
                  >
                    <Icon name="spinner" loading={this.state.runLoading} />
                    {this.state.runStop}
                  </Button>
                </Grid.Row>
                <br />
                <Grid.Row verticalAlign="middle">
                  <Button
                    loading={this.state.singleLoading}
                    disabled={this.state.disableSingle}
                    fluid
                    color="yellow"
                    onClick={this.handleClickSingle}
                  >
                    {this.state.single}
                  </Button>
                </Grid.Row>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
        <Segment basic disabled={this.state.functionDisable}>
          <Form>
            <Form.Group widths="equal">
              <Form.Field
                disabled={this.state.formDisabled}
                onChange={this.handleChange}
                control={Select}
                options={memLengthOptions}
                label={{
                  children: "Memory Length",
                  htmlFor: "form-select-memory-length"
                }}
                placeholder="1M Points"
                search
                searchInput={{ id: "form-select-memory-length" }}
              />
              <Form.Field
                disabled={this.state.formDisabled}
                onChange={this.handleChange}
                control={Select}
                options={vRangeOptions}
                label={{
                  children: "Vertical Range",
                  htmlFor: "form-select-vertical-range"
                }}
                placeholder="+- 1V"
                search
                searchInput={{ id: "form-select-vertical-range" }}
              />
              <Form.Field
                disabled={this.state.formDisabled}
                onChange={this.handleChange}
                control={Select}
                options={sampleRateOptions}
                label={{
                  children: "Sampleing Rate",
                  htmlFor: "form-select-sampleing-rate"
                }}
                placeholder="80MSPS"
                search
                searchInput={{ id: "form-select-sampleing-rate" }}
              />
              <Form.Field
                disabled={this.state.formDisabled}
                onChange={this.handleChange}
                control={Select}
                options={triggerSourceOptions}
                label={{
                  children: "Trigger Source",
                  htmlFor: "form-select-trigger-source"
                }}
                placeholder="Internal"
                search
                searchInput={{ id: "form-select-trigger-source" }}
              />
            </Form.Group>
          </Form>
        </Segment>
      </div>
    );
  }
}

export default App;
